import gui.GameWindow;

import java.awt.EventQueue;


public class Main {

	public static void main (String[] args){

		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {

				GameWindow window = new GameWindow(); /// Create a new window with a new GameState
				
				window.setVisible(true);
				window.requestFocusInWindow();
			}

		});

	}
}
