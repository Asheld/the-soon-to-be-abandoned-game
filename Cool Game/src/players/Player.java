package players;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.List;

import monsters.Monster;

import painter.MovingSprite;
import terrain.Block;

/**
 * The player
 * @author Asheld
 *
 */
public class Player extends MovingSprite {
	
	/**
	 * Basic stats
	 * HP: The current health points of the player
	 * Attack: The offensive power of the player
	 * Defense: The defensive power of the player
	 */
	private int hp, maxHP, attack, defense;
	
	/**
	 * The player
	 * @param Name of the player
	 * @param X coordinate of the player
	 * @param Y coordinate of the player
	 */
	public Player(int xPosition, int yPosition, int hp, int attack, int defense) {
		super(xPosition, yPosition);
		
		this.hp = hp;
		this.maxHP = hp;
		this.attack = attack;
		this.defense = defense;
		
		//// Only temporarily until we get something else to placehold.
		BufferedImage player = new BufferedImage(50,50, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = player.createGraphics();
		g2d.setColor(Color.black);
		g2d.fillOval(0, 0, 50, 50);
		g2d.setColor(Color.white);
		g2d.fillOval(10, 15, 10, 20);
		g2d.fillOval(30, 15, 10, 20);
		g2d.dispose();
		setSprite(player);
		
	}
	
	/**
	 * Handles what happens when the player tries to attack
	 */
	public void handleAttacking(List<Monster> monsterList) {

		int width = getWidth() / 2, targetX = 0;

		switch (getDirection()) {

		case LEFT:
			width = -width;
			targetX = -5;
			break;
		case RIGHT:
			targetX = 5;
			break;

		default:
			break;

		}

		Monster m = findMonster(this, width, targetX, monsterList);
		if (m != null) {

			if (m.getRect().getCenterY() > getRect().getCenterY()) {
				grapple(m);
			
			} else {
				attack(m);
			}
			
			
			if (m.getHP() <= 0) {
				monsterList.remove(m);
			}
			
		} 


	}

	/**
	 * Attack the selected monster
	 * @param m The target monster
	 * @return The remaining health of the monster
	 */
	public int attack(Monster m) {

		double damageRoll = Math.random() * 100;

		if (damageRoll < 10 && m.checkHead()) { /// Behead

			m.setHP(-1);

		} else if (damageRoll < 25 && m.checkLegs()) { /// Leg damage (-speed)

			m.setHP(m.getHP() - m.getMaxHP() / 4 - m.getDefense());

			if (m.getSpeed() > 1) {
				m.setSpeed(m.getSpeed() / 2);
			} else { m.setSpeed(0); }

		} else if (damageRoll < 35 && m.checkArms()) { /// Arm damage (-offense/defense)

			m.setHP(m.getHP() - m.getMaxHP() / 6 - m.getDefense()); 

			if (m.getAttack() > 1) {
				m.setAttack(m.getAttack() / 2);
			}

			if (m.getDefense() > 1) {
				m.setDefense(m.getDefense() / 4);
			}

		} else if (damageRoll < 85) { /// Body damage

			m.setHP(m.getHP() - m.getMaxHP() / 10 - m.getDefense());

		} else { /// Scratch

			m.setHP(m.getHP() - m.getMaxHP() / 50);

		}

		System.out.println("HP: " + m.getHP());

		return m.getHP();

	}
	
	/**
	 * Grapple with the monster, only possible if player stands above it
	 * @param m The target
	 * @return The remaining health of the monster
	 */
	public int grapple(Monster m) {
		
		double damageRoll = Math.random() * 100;
		
		if (damageRoll < 10) {
			m.setCoolDown(0);
		} else if (damageRoll < 80) {
			m.setHP(m.getHP() - m.getMaxHP() / 10);
			m.setCoolDown(30);
		} else {
			m.setHP(m.getHP() - m.getMaxHP() / 15);
		}
		
		return m.getHP();
	}
	
	/**
	 * Handles what happens when the player attempts to dig a tile
	 * @param k The key code of the key press
	 */
	public void handleDigging(int k, List<Block> terrainList) { 

		switch(k) {

		case KeyEvent.VK_UP:
			dig(Direction.UP, terrainList);
			break;

		case KeyEvent.VK_DOWN:
			dig(Direction.DOWN, terrainList);
			break;

		case KeyEvent.VK_LEFT:
			dig(Direction.LEFT, terrainList);
			break;

		case KeyEvent.VK_RIGHT:
			dig(Direction.RIGHT, terrainList);
			break;

		}
	}
	
	/**
	 * Attempt to dig a tile
	 * @param sprite
	 * @param width 
	 * @param height
	 * @param targetX
	 * @param targetY
	 * @return
	 */
	public boolean dig(Direction direction, List<Block> terrainList) {
		
		Rectangle rect = getHitBox();
		
		int xOffset = 0, yOffset = 0, targetX = 0, targetY = 0;
		
		switch (direction) {
		
		case UP:
			yOffset = -getHeight() / 2;
			targetY = -15;
			break;
			
		case DOWN:
			yOffset = getHeight() / 2;
			targetY = 5;
			break;
			
		case LEFT:
			xOffset = -getWidth() / 2;
			targetX = -5;
			break;
			
		case RIGHT:
			xOffset = getWidth() / 2;
			targetY = 5;
			break;
			
			default: break;
		}
		
		synchronized(terrainList) {
			for (Block b : terrainList) {
				if (b.getRect().contains(
						rect.getCenterX() + xOffset + targetX, 
						rect.getCenterY() + yOffset + targetY) &&
						b.getDiggability() == true) {
					terrainList.remove(b);
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Find whether or not there is a monster in attacking distance
	 * @param player The player
	 * @param width The width of the player (negative if left)
	 * @param targetX The range of the player (negative if left)
	 * @return The results
	 */
	public Monster findMonster(Player player, int width, int targetX, List<Monster> monsterList) {

		Rectangle rect = player.getHitBox();

		synchronized(monsterList) {
			for (Monster m : monsterList) {
				if (m.getRect().contains(   /// Check if the monster is left/right to the player
						rect.getCenterX() + width + targetX,
						rect.getCenterY()) || 
					
					m.getRect().contains( /// Check if the monster is below the player
							rect.getCenterX(), 
							rect.getCenterY() + rect.height / 2 + 5)) {
					return m; /// Return the monster if either of them is true
				}
			}
		}

		return null; /// Return nothing if no monsters are found in range
	}
	
	/**
	 * Get the current health points of the player
	 * @return hp The remaining health points
	 */
	public int getHP() {
		return hp;
	}
	
	/**
	 * Get the maximum health points of the player
	 * @return maxHP The maximum amount of health points
	 */
	public int getMaxHP() {
		return maxHP;
	}

	/**
	 * Get the current offensive power of the player 
	 * @return attack The offensive power
	 */
	public int getAttack() {
		return attack;
	}

	/**
	 * Get the current defensive power of the player
	 * @return defense The defensive power
	 */
	public int getDefense() {
		return defense;
	}
	
	/**
	 * Increase or decrease the health of the player
	 * @param health New health amount
	 */
	public void setHP(int health) {
		hp = health;
	}
	
	/**
	 * Increase or decrease the current offensive power of the player
	 * @param attack New offensive power
	 */
	public void setAttack(int attack) {
		this.attack = attack;
	}
	
	/**
	 * Increase or decrease the current defensive power of the player
	 * @param defense New defensive power
	 */
	public void setDefense(int defense) {
		this.defense = defense;
	}

}
