package players;

/**
 * An enumerator used to represent the current direction of the sprites
 * @author Asheld
 *
 */
public enum Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT;

	/// Not sure if this method is done properly...
	public static Direction reverse (Direction direction) {

		switch(direction) {

		case UP:
			return DOWN;

		case DOWN:
			return UP;

		case LEFT:
			return RIGHT;

		case RIGHT:
			return LEFT;

		default:
			break;
		}
		
		return direction;

	}

}
