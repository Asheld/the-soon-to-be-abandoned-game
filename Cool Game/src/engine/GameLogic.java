package engine;

import java.awt.event.KeyEvent;
import java.util.HashSet;

import monsters.Monster;

import players.Direction;
import players.Player;

import gui.GameWindow;
import gui.KeyboardInput;
import gui.KeyboardInput.KeyState;
import threading.Tickable;

/**
 * Responsible for updating the game state and processing all logic in the game.
 * @author Steven
 *
 */
public class GameLogic implements Tickable{

	private GameState gameState;

	private final HashSet<Integer> controls = new HashSet<Integer>(){{
		add(KeyEvent.VK_UP);
		add(KeyEvent.VK_RIGHT);
		add(KeyEvent.VK_DOWN);
		add(KeyEvent.VK_LEFT);
		add(KeyEvent.VK_Z);
		add(KeyEvent.VK_X);
	}};

	public GameLogic(GameState gameState){
		this.gameState = gameState;
	}

	/**
	 * Update loop
	 */
	@Override
	public void tick() {
		if(GameState.running == true) {
			processKeys();
			processAI();
			processCollisions();


		}
	}

	/**
	 * Iterates over the keys to check which ones are pressed and then handles them accordingly.
	 */
	private void processKeys() {
		KeyState[] keyState = KeyboardInput.getKeyState();
		for(Integer i : controls){

			if (keyState[i] == KeyState.ONCE) {

				if (i == KeyEvent.VK_Z) {
					gameState.getPlayer().handleAttacking(gameState.getMonsters());
					if(gameState.getMonsters().size() <= 0) {
						gameState.getStageManager().nextStage();
					}
				}

				if (keyState[KeyEvent.VK_X] == KeyState.PRESSED) {

					gameState.getPlayer().handleDigging(i, gameState.getTerrain());

				} else if (keyState[KeyEvent.VK_UP] == KeyState.PRESSED && i == KeyEvent.VK_X) {

					gameState.getPlayer().handleDigging(KeyEvent.VK_UP, gameState.getTerrain());

				} else if (keyState[KeyEvent.VK_DOWN] == KeyState.PRESSED && i == KeyEvent.VK_X) {

					gameState.getPlayer().handleDigging(KeyEvent.VK_DOWN, gameState.getTerrain());

				} else if (keyState[KeyEvent.VK_LEFT] == KeyState.PRESSED && i == KeyEvent.VK_X) {

					gameState.getPlayer().handleDigging(KeyEvent.VK_LEFT, gameState.getTerrain());

				} else if (keyState[KeyEvent.VK_RIGHT] == KeyState.PRESSED && i == KeyEvent.VK_X) {

					gameState.getPlayer().handleDigging(KeyEvent.VK_RIGHT, gameState.getTerrain());

				}
			}

			if (keyState[i] == KeyState.PRESSED || keyState[i] == KeyState.ONCE){

				if (keyState[KeyEvent.VK_X] == KeyState.RELEASED) {
					handleKeyEvent(i);
				}
			}
		}



	}

	/**
	 * Detect and handle collisions that happen
	 */
	private void processCollisions() {

		if (!gameState.getCollision(gameState.getPlayer(), Direction.UP, (int) gameState.getPlayer().getVeloY(), true)) {
			gameState.getPlayer().update(GameWindow.TPS);
		} else  {
			gameState.getPlayer().setVeloY(0);
			gameState.getPlayer().update(GameWindow.TPS);
		}

		synchronized(gameState.getMonsters()) {
			for (Monster m : gameState.getMonsters()) {

				if (!gameState.getCollision(m, Direction.UP, (int) m.getVeloY(), false)) {
					m.update(GameWindow.TPS);
				} else {
					m.setVeloY(0);
					m.update(GameWindow.TPS);
				}

			}
		}

	}

	/**
	 * Handle the AI for enemies
	 */
	private void processAI() {

		synchronized(gameState.getMonsters()) {
			for (Monster m : gameState.getMonsters()) {

				switch(m.getAIMode()) {

				case STANDBY: /// Do nothing until player enters detection range

					m.standBy(gameState.getPlayer());

					break;

				case OFFENSIVE: /// If player is not in range, wander around looking, if player is in range, seek and kill

					if (inSight(m)) {

						if (inAttackRange(m) && !playerIsDead(gameState.getPlayer())) {

							if (m.getCoolDown() == 0) {
								m.attack(gameState.getPlayer()); 
							}

							if (playerIsDead(gameState.getPlayer())) {
								gameState.getStageManager().setGameOver();
							}

						} else {
							chaseTarget(m);
						}
					} else {
						patrolSurroundings(m);
					}
					break;

				case DEFENSIVE: //// Not doing anything with it till later

					break;

				}

			}
		} 

	}


	/**
	 * Handles the key presses and updates the game state accordingly.
	 * @param k The key code of the key press
	 */
	private void handleKeyEvent(int k){
		switch(k){
		case KeyEvent.VK_UP:
			if (gameState.checkGround(gameState.getPlayer())) {
				gameState.getPlayer().move(Direction.UP);
			}
			break;
		case KeyEvent.VK_LEFT:
			if (!gameState.getCollision(gameState.getPlayer(), Direction.LEFT, gameState.getPlayer().getSpeed(), true)) {
				gameState.getPlayer().move(Direction.LEFT);

			}
			gameState.getPlayer().setDirection(Direction.LEFT);
			break;
		case KeyEvent.VK_RIGHT:
			if (!gameState.getCollision(gameState.getPlayer(), Direction.RIGHT, gameState.getPlayer().getSpeed(), true)) {
				gameState.getPlayer().move(Direction.RIGHT);
			}
			gameState.getPlayer().setDirection(Direction.RIGHT);
			break;
		}
	}

	/**
	 * Checks if they are in sight
	 * @param m The monster
	 * @return The results(true/false)
	 */
	private boolean inSight(Monster m) {

		if (m.detectPlayer(gameState.getPlayer(),  m.getDirection(), m.getSensitivity()) || 
				m.detectPlayer(gameState.getPlayer(),  Direction.reverse(m.getDirection()), m.getSensitivity())) {
			return true;
		}

		return false;
	}

	/**
	 * Checks if they are in attack range
	 * @param m The monster
	 * @return The results(true/false)
	 */
	private boolean inAttackRange(Monster m) {

		if (m.findPlayer(gameState.getPlayer(), -m.getWidth() / 2, -m.getAttackRange()) != null ||
				m.findPlayer(gameState.getPlayer(), m.getWidth() / 2, m.getAttackRange()) != null) {

			return true;
		}

		return false;
	}

	/**
	 * Chase the player, jump if an obstacle is in the way
	 * @param m
	 */
	private void chaseTarget(Monster m) {
		Direction direction = targetDirection(m.getX(), gameState.getPlayer().getX());
		if(direction == Direction.LEFT || direction == Direction.RIGHT){
			if (!gameState.getCollision(m, direction, m.getSpeed(), false)) {
				m.move(direction);
			}

			else if (m.getY() > gameState.getPlayer().getY() && gameState.checkGround(m)){
				m.move(Direction.UP);
			}
		}
	}

	/**
	 * Patrol the surroundings until further notice 
	 * @param m The monster
	 */
	private void patrolSurroundings(Monster m) {

		if (!gameState.getCollision(m, m.getDirection(), m.getSpeed(), false)) {
			m.move(m.getDirection());
		} else if (gameState.getCollision(m, Direction.reverse(m.getDirection()), m.getSpeed(), false)) {
			m.move(Direction.UP);
		} else {
			m.setDirection(Direction.reverse(m.getDirection()));
		}
	}

	/**
	 * Get the relative direction (left/right) to the target
	 * @param x Location of the first target
	 * @param x2 Location of the second target
	 * @return Direction The direction
	 */
	private Direction targetDirection(int x, int x2) {

		if (x > x2) {

			return Direction.LEFT;

		} else if (x < x2) {

			return Direction.RIGHT;
		}


		return Direction.DOWN;
	}

	/**
	 * Checks if the player is still alive
	 * @param p Player
	 * @return The answer(true/false)
	 */
	private boolean playerIsDead(Player p) {

		if (p.getHP() <= 0) {
			return true;
		}

		return false;
	}

}
