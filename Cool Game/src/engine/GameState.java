package engine;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import monsters.Monster;

import painter.DrawPanel;
import painter.Sprite;
import players.Direction;
import players.Player;
import stages.StageManager;
import terrain.Block;

/**
 * For dealing with the game state
 * @author Asheld
 *
 */
public class GameState {
	
	/**
	 * List of players in the game
	 */
	private List<Player> playerList = Collections.synchronizedList(new ArrayList<Player>());
	
	/**
	 * List of monsters in the game
	 */
	private List<Monster> monsterList = Collections.synchronizedList(new ArrayList<Monster>());
	
	/**
	 * List of terrain blocks in the game
	 */
	private List<Block> terrainList = Collections.synchronizedList(new ArrayList<Block>());
	
	/**
	 * Current resolution of the game
	 */
	private Dimension windowSize = new Dimension(800,600);
	
	/**
	 * The stage manager
	 */
	private StageManager stageManager;
	
	/**
	 * The panel of which the game is drawn on
	 */
	private DrawPanel drawPanel;
	
	/**
	 * Responsible for determining whether or not the game is currently running, can be switched to false for pausing
	 */
	public static volatile boolean running = true;
	
	/**
	 * For detecting collisions with the floor, ceiling, walls.
	 * @param sprite The sprite to check
	 * @param direction The direction to check
	 * @param velocity The speed the sprite is moving at
	 * @param player Whether or not the collision check is being done on the player
	 * @return The result (true/false)
	 */
	public boolean getCollision(Sprite sprite, Direction direction, int velocity, boolean player) {
		
		Rectangle rect = sprite.getHitBox();
		
		switch(direction) {
		case UP:
			rect.setLocation(rect.x, rect.y + velocity);
			synchronized(terrainList) {
				for (Block b : terrainList) {			
					if (rect.intersects(b.getRect())) {
						return adjustPos(rect, sprite, b);					
					}
				}
			}
			
			break;
		case LEFT:
			rect = new Rectangle(sprite.getX() - velocity, sprite.getY(), sprite.getWidth(), sprite.getHeight());
			break;
		case RIGHT:
			rect = new Rectangle(sprite.getX() + velocity, sprite.getY(), sprite.getWidth(), sprite.getHeight());
			break;
		default:
			break;
		}
		
		if (rect.x + velocity > stageManager.getStageSize().width && direction != Direction.UP || rect.x - 50 < 0) {
			return true;
		}

		synchronized(terrainList) {
			for (Block b : terrainList) {			
				if (rect.intersects(b.getRect())) {
					return true;					
				}
			}
		}
		return false;
	}
	
	/**
	 * Check whether or not the sprite is grounded
	 * @param s
	 * @return
	 */
	public boolean checkGround(Sprite s) {
		Rectangle rect = s.getRect();
		
		synchronized(terrainList) {
			for (Block b : terrainList) {
				if (b.getRect().contains(rect.getCenterX(), rect.getCenterY() + rect.getHeight() / 2 + 5)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	private boolean adjustPos(Rectangle rect, Sprite sprite, Sprite b){
		
		/// If the intersecting target is above then set the roof to the lowest value of the rectangle
		if (rect.getCenterY() > b.getRect().getCenterY()) {
		sprite.setY((int)b.getRect().getMinY() + sprite.getHeight());
		} 
		
		/// If the intersecting target is below then set the floor to the highest value of the rectangle
		else if (rect.getCenterY() < b.getRect().getCenterY()) {
			sprite.setY((int)b.getRect().getMaxY() - sprite.getHeight() * 2);
		}
		return true;
	}
	
	/**
	 * Add a player to the GameState.
	 * @param player A new player
	 */
	public void addPlayer(Player player) {
		playerList.add(player);
	}
	
	/**
	 * Add a new monster to the GameState
	 * @param monster A new monster
	 */
	public void addMonster(Monster monster) {
		monsterList.add(monster);
	}
	
	/**
	 * Add a new block of terrain to the GameState
	 * @param terrain A new tile
	 */
	public void addTerrain(Block terrain) {
		terrainList.add(terrain);
	}
	
	/**
	 * Set the stage manager
	 * @param stageManager
	 */
	public void addStageManager(StageManager stageManager) {
		this.stageManager = stageManager;
	}
	
	/**
	 * Set the draw panel
	 * @param drawPanel
	 */
	public void addDrawPanel(DrawPanel drawPanel) {
		this.drawPanel = drawPanel;
	}
	
	/**
	 * Obtain the list of players from the GameState.
	 * @return playerList A list of players
	 */
	public List<Player> getPlayers() {
		return playerList;
	}
	
	/**
	 * Obtain the list of monsters from the GameState.
	 * @return monsterList A list of monsters
	 */
	public List<Monster> getMonsters() {
		return monsterList;
	}
	
	/**
	 * Obtain the terrain in the GameState
	 * @return terrainList A list of terrain
	 */
	public List<Block> getTerrain() {
		return terrainList;
	}
	
	/**
	 * Get the first player in the playerList
	 * @return the Player in index 0
	 */
	public Player getPlayer() {
		if (playerList.size() > 0) {
		return playerList.get(0);
		}
		
		return new Player(0, 0, 0, 0, 0);
	}
	
	/**
	 * Get the current resolution of the window
	 * @return windowSize The current resolution
	 */
	public Dimension getWindowSize() {
		return windowSize;
	}
	
	/**
	 * Get the stage manager
	 * @return
	 */
	public StageManager getStageManager() {
		return stageManager;
	}
	
	/**
	 * Get the draw panel
	 * @return
	 */
	public DrawPanel getDrawPanel() {
		return drawPanel;
	}
	

}
