package stages;

import java.awt.Dimension;
import java.io.InputStream;
import java.util.List;

import monsters.Debugmon;
import monsters.Monster;
import players.Player;
import terrain.Block;
import terrain.Grass;
import terrain.Ground;
import terrain.Underground;

import engine.GameState;

public class StageManager {

	private GameState gameState;
	
	private int currentStage = 0;
	private int currentPart = 0;
	private Dimension stageSize = new Dimension(0,0);
	
	public StageManager(GameState gameState) {
		this.gameState = gameState;
	}
	
	public void constructStage(int stageNumber, int stagePart) {
		
		currentStage = stageNumber;
		currentPart = stagePart;
		int x = -100, y = 0;
		
		try {
			
			
			InputStream stream = getClass().getResourceAsStream("/resources/" + stageNumber + "-"  + stagePart + ".txt");
			
			while (stream.available() > 1) {
			char character = (char) stream.read();
			
			String key = String.valueOf(character);
			
			if (key.equals("P")) {
				Player p = new Player(x,y,100,10,10);
				gameState.addPlayer(p);
			} else if (key.equals("G")) {
				Grass g = new Grass(x,y);
				gameState.addTerrain(g);
			} else if (key.equals("g")) {
				Ground g = new Ground(x,y);
				gameState.addTerrain(g);
			} else if (key.equals("u")) {
				Underground u = new Underground(x,y);
				gameState.addTerrain(u);
			} else if (key.equals("D")) {
				Debugmon debugmon = new Debugmon(x,y);
				gameState.addMonster(debugmon);
			} else if (key.equals(";")) {
				x = -100;
				y += 50;
			}
			
			x += 50;
			
			stageSize.width = stageSize.width > x ? stageSize.width : x - 50;
			stageSize.height = stageSize.height > y ? stageSize.height : y;

			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void deconstructStage() {
		
		List<Player> playerList = gameState.getPlayers();
		synchronized(gameState.getPlayers()) {
			playerList.clear();
		}
		
		List<Monster> monsterList = gameState.getMonsters();
		synchronized(monsterList) {
			monsterList.clear();
		}
		
		List<Block> terrainList = gameState.getTerrain();
		synchronized(terrainList) {
			terrainList.clear();
		}
		
	}
	
	public void setGameOver() {
		gameState.getDrawPanel().setGameOver();
	}
	
	public void nextStage() { 
		currentStage += 1;
		currentPart = 1;
		deconstructStage();
		constructStage(currentStage, currentPart);
	}
	
	public Dimension getStageSize() {
		return stageSize;
	}
	
	public int getCurrentStage() {
		return currentStage;
	}
	
	
}
