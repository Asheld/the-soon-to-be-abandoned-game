package stages;

import monsters.AIMode;
import monsters.Debugmon;
import players.Player;
import terrain.Ground;
import engine.GameState;

public class StageOne {
	
	private GameState state;
	
	public StageOne(GameState state) {
		this.state = state;
		init();
	}
	
	/**
	 * Initialize the stage
	 */
	private void init() {
		
		/// Add Player
		Player player = new Player(0, state.getWindowSize().height - 200, 100, 10, 10);
		state.addPlayer(player);
		
		/// Add monsters
		Debugmon debugmon = new Debugmon(200, state.getWindowSize().height - 200);
		debugmon.setAI(AIMode.STANDBY);
		state.addMonster(debugmon);
		
		
		/// Add terrain
		createMap();
	}

	/**
	 * Create the map of the stage
	 */
	private void createMap() {
		
		for (int y = state.getWindowSize().height - 25; y < 1000; y += 50) {
		for (int x = 0; x < 2400; x += 50) {
			Ground ground = new Ground(x, y);
			state.addTerrain(ground);
		}
		}
		
		/*for (int x = 0; x < 2400; x += 50) {
			Ground ground = new Ground(x, state.getWindowSize().height - 200);
			state.addTerrain(ground);
		}*/
		
		
		Ground wall = new Ground(state.getWindowSize().width - 50, state.getWindowSize().height - 75);
		state.addTerrain(wall);
		
		Ground wall2 = new Ground(state.getWindowSize().width - 50, state.getWindowSize().height - 125);
		state.addTerrain(wall2);
		
		Ground wall3 = new Ground(0, state.getWindowSize().height - 100);
		state.addTerrain(wall3);
		
		Ground floatingGround = new Ground(state.getWindowSize().width - 400, state.getWindowSize().height - 300);
		state.addTerrain(floatingGround);

	}
}
