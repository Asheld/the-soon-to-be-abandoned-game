package monsters;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 * Monster created for debug purposes
 * @author Asheld
 *
 */
public class Debugmon extends Monster {
	
	public Debugmon(int x, int y) {
		super(x, y, "Debug Monster", 100, 10, 10, 100, 5, AIMode.STANDBY);
		
		
		setSpeed(2);
		
		//// Only temporarily until we get something else to placehold.
		BufferedImage debugmon = new BufferedImage(50,50, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = debugmon.createGraphics();
		g2d.setColor(new Color(200,0,35));
		g2d.fillOval(0, 0, 50, 50);
		g2d.setColor(Color.black);
		g2d.fillOval(10, 15, 10, 20);
		g2d.fillOval(30, 15, 10, 20);
		g2d.dispose();
		setSprite(debugmon);
		
	}

}
