package monsters;

import java.awt.Rectangle;

import painter.MovingSprite;
import players.Direction;
import players.Player;

/**
 * Basic template for monsters.
 * @author Asheld
 *
 */
public abstract class Monster extends MovingSprite {

	/**
	 * The name of the monster
	 */
	private String name = "";

	/**
	 * Basic stats
	 * HP: The health points of the monster
	 * Attack: The offensive power of the monster
	 * Defense: The defensive power of the monster
	 * Sensitivity: The detection range of the monster
	 * Attack Range: The reach of the monster
	 */
	private int hp, maxHP, attack, defense, sensitivity, attackRange;

	/**
	 * Body part check
	 */
	private boolean hasArms = true, hasLegs = true, hasHead = true;
	
	private AIMode ai;

	public Monster(int x, int y, String name, int hp, int attack, int defense, int sensitivity, int attackRange, AIMode ai) {
		super(x, y);
		this.hp = hp;
		this.maxHP = hp;
		this.attack = attack;
		this.defense = defense;
		this.sensitivity = sensitivity;
		this.attackRange = attackRange;
		this.ai = ai;
		
		////setDirection(Direction.LEFT);
	}

	/**
	 * Get the name of the monster
	 * @return name The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the current health points of the monster 
	 * @return hp The remaining health points
	 */
	public int getHP() {
		return hp;
	}
	
	/**
	 * Get the maximum health points of the monster
	 * @return maxHP The maximum amount of health points
	 */
	public int getMaxHP() {
		return maxHP;
	}

	/**
	 * Get the current offensive power of the monster 
	 * @return attack The offensive power
	 */
	public int getAttack() {
		return attack;
	}

	/**
	 * Get the current defensive power of the monster
	 * @return defense The defensive power
	 */
	public int getDefense() {
		return defense;
	}
	
	/**
	 * Get the current sensitivity of the monster
	 * @return
	 */
	public int getSensitivity() {
		return sensitivity;
	}
	
	public int getAttackRange() {
		return attackRange;
	}
	
	public AIMode getAIMode() {
		return ai;
	}
	
	/**
	 * Check if this monster has a head
	 * @return hasHead The answer
	 */
	public boolean checkHead() {
		return hasHead;
	}
	
	/**
	 * Check if this monster has legs
	 * @return hasLegs The answer
	 */
	public boolean checkLegs() {
		return hasLegs;
	}
	
	/**
	 * Check if this monster has arms
	 * @return hasArms The answer
	 */
	public boolean checkArms() {
		return hasArms;
	}
	
	/**
	 * Increase or decrease the health of the monster
	 * @param health New health amount
	 */
	public void setHP(int health) {
		hp = health;
	}
	
	/**
	 * Increase or decrease the current offensive power of the monster
	 * @param attack New offensive power
	 */
	public void setAttack(int attack) {
		this.attack = attack;
	}
	
	/**
	 * Increase or decrease the current defensive power of the monster
	 * @param defense New defensive power
	 */
	public void setDefense(int defense) {
		this.defense = defense;
	}
	
	/**
	 * Increase or decrease the current sensitivity of the monster
	 * @param sensitivity New sensitivity
	 */
	public void setSensitivity(int sensitivity) {
		this.sensitivity = sensitivity;
	}
	
	public void setAI(AIMode ai) {
		this.ai = ai;
	}
	
	/**
	 * Attack the selected player
	 * @param p The target player
	 * @return The remaining health of the player
	 */
	public int attack(Player p) {

		double damageRoll = Math.random() * 100;

		if (damageRoll < 10) {

			p.setHP(-1);

		} else if (damageRoll < 25) {

			p.setHP(p.getHP() - p.getMaxHP() / 4 - p.getDefense());

			if (p.getSpeed() > 1) {
				p.setSpeed(p.getSpeed() / 2);
			} else { p.setSpeed(1); }

		} else if (damageRoll < 35) {

			p.setHP(p.getHP() - p.getMaxHP() / 6 - p.getDefense()); 

			if (p.getAttack() > 1) {
				p.setAttack(p.getAttack() / 2);
			}

			if (p.getDefense() > 1) {
				p.setDefense(p.getDefense() / 4);
			}

		} else if (damageRoll < 85) {

			p.setHP(p.getHP() - p.getMaxHP() / 10 - p.getDefense());

		} else {

			p.setHP(p.getHP() - p.getMaxHP() / 50);

		}
		
		setCoolDown(60);

		return p.getHP();

	}
	
	/**
	 * Detect whether or not there is a player in the detection range.
	 * @param m The monster
	 * @param direction The direction to detect
	 * @param radius The detection range
	 * @return The results (true/false)
	 */
	public boolean detectPlayer(Player p, Direction direction, int radius) {
		
		Rectangle rect = getRect();
		
		switch (direction) {
		case LEFT:
			rect.setBounds(rect.x - rect.width - radius , rect.y - rect.height - radius, rect.width + radius, (rect.height + radius) * 2);

			break;

		case RIGHT:
			rect.setBounds(rect.x, rect.y - rect.height - radius, rect.width + radius, (rect.height + radius) * 2);
			break;

		default:	
			break;
		}
		
		if (rect.intersects(p.getRect())) {
			return true;
		}

		return false;
	}
	
	/**
	 * Find whether or not there is a player in attacking distance
	 * @param monster The monster
	 * @param width The width of the monster (negative if left)
	 * @param targetX The range of the monster (negative if left)
	 * @return The results
	 */
	public Player findPlayer(Player p, int width, int targetX) {
	
		Rectangle rect = getHitBox();

		if (p.getRect().contains(
				rect.getCenterX() + width + targetX, 
				rect.getCenterY())) {
			return p;
		}

		return null;
	}
	

	public void standBy(Player p) { 
		if (detectPlayer(p, getDirection(), getSensitivity())) {
			setAI(AIMode.OFFENSIVE);
		}
	}

}
