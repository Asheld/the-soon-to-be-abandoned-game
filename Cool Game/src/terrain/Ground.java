package terrain;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

public class Ground extends Block {

	public Ground(int x, int y) {
		super(x, y, true);

		ImageIcon icon = new ImageIcon(getClass().getResource("/resources/dirt.png"));
		/*	BufferedImage tileImage = new BufferedImage(50,50,BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = tileImage.createGraphics();
		g2d.setColor(Color.black);
		g2d.fillRect(0, 0, 50, 50);
		g2d.dispose();*/

		BufferedImage tileImage = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = tileImage.createGraphics();
		g2d.drawImage(icon.getImage(), 0,0,null);
		g2d.dispose();

		setSprite(tileImage);

	}

}
