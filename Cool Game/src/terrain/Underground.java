package terrain;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

public class Underground extends Block {

	public Underground(int x, int y) {
		super(x, y, false);
		ImageIcon icon = new ImageIcon(getClass().getResource("/resources/underground.png"));
		BufferedImage tileImage = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = tileImage.createGraphics();
		g2d.drawImage(icon.getImage(), 0,0,null);
		g2d.dispose();

		setSprite(tileImage);
	}

}
