package terrain;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

public class Grass extends Block {

	public Grass(int x, int y) {
		super(x, y, true);

		ImageIcon icon = new ImageIcon(getClass().getResource("/resources/ground.png"));
		
		BufferedImage tileImage = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = tileImage.createGraphics();
		g2d.drawImage(icon.getImage(), 0,0,null);
		g2d.dispose();

		setSprite(tileImage);
	}

}
