package terrain;

import painter.Sprite;

public abstract class Block extends Sprite {
	
	private boolean diggability;

	public Block(int x, int y, boolean diggability) {
		super(x, y);
		this.diggability = diggability;
		// TODO Auto-generated constructor stub
	}

	public void setDiggability(boolean diggability) {
		this.diggability = diggability;
	}
	
	public boolean getDiggability() {
		return diggability;
	}
	
}
