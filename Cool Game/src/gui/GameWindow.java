package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

import painter.DrawPanel;
import stages.StageManager;
import stages.StageOne;
import threading.TickThread;
import engine.GameLogic;
import engine.GameState;

/**
 * Game window which will serve as a window for the game.
 * @author Asheld
 *
 */
@SuppressWarnings("serial")
public class GameWindow extends JFrame {
	
	private GameState gameState;
	private DrawPanel drawPanel;
	private StageManager stageManager;
	
	public static final int TPS = 60;
	
	private Dimension windowRes = new Dimension(800,600);
	
	public GameWindow() {
		setTitle("The Soon To Be Abandoned Game (ver 0.0)");
		setIconImage(new ImageIcon(getClass().getResource("/resources/blankIcon.png")).getImage());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setBounds(100,100,windowRes.width,windowRes.height);
		
		init();
	}

	/**
	 * Initialize the game.
	 */
	private void init() {
		
		gameState = new GameState();
		stageManager = new StageManager(gameState);
		gameState.addStageManager(stageManager);
		
		KeyboardInput keyListener = new KeyboardInput();
		addKeyListener(keyListener);

		drawPanel = new DrawPanel(gameState);
		TickThread drawThread = new TickThread(drawPanel, TPS);
		add(drawPanel);
		drawThread.start();
		
		gameState.addDrawPanel(drawPanel);

		TickThread t = new TickThread(new GameLogic(gameState), TPS);
		t.start();

		TickThread keyPoller = new TickThread(keyListener, TPS);
		keyPoller.start();

	}

}
