package painter;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

/**
 * Abstract class for all sprites used in the game
 * @author Asheld
 *
 */
public abstract class Sprite {

	/**
	 * Location, width, height of the sprite
	 */
	private int x, y, width, height;

	/**
	 * The image for the sprite
	 */
	private BufferedImage sprite;
	
	/**
	 * Create a new sprite
	 * @param x location
	 * @param y location
	 */
	public Sprite(int x, int y) {
		this.x = x;
		this.y = y;
		
		/// Temporary until we get proper sprites
		width = 0;
		height = 0; 
	}

	/**
	 * Draw the sprite
	 * @param g
	 */
	public void drawSprite(Graphics2D g2d) {
		g2d.drawImage(sprite,x,y,null);
	}
	
	public void setSprite(BufferedImage image) {
		sprite = image;
		width = sprite.getWidth();
		height = sprite.getHeight();
	}
	
	/**
	 * Return the width of the sprite.
	 * @return width
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * Return the height of the sprite.
	 * @return height
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Return the x position of the sprite.
	 * @return x position
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Return the y position of the sprite.
	 * @return y position
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Get a rectangle representing the area the sprite is currently positioned at
	 * @return Rectangle
	 */
	public Rectangle getRect() {
		return new Rectangle(x, y, width, height);
	}
	
	/**
	 * Get the hitbox of the sprite
	 * @return hitbox
	 */
	public Rectangle getHitBox() {
		return new Rectangle(x + 5, y + 10, width - 10, height - 10);
	}
	
	/**
	 * Set a new x position for the sprite
	 * @param x position
	 */
	public void setX(int x) {
		this.x = x;
	}
	
	/**
	 * Set a new y position for the sprite
	 * @param y position
	 */
	public void setY(int y) {
		this.y = y;
	}
	
}
