package painter;

import java.awt.Graphics2D;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class GameOverMenu {

	private JButton retryButton, returnButton;
	
	private ImageIcon icon = new ImageIcon(getClass().getResource("/resources/gameOverScreen.png"));

	private int x, y;


	public GameOverMenu(JPanel panel, ActionListener listener, int x, int y) {

		retryButton = new JButton(new ImageIcon(getClass().getResource("/resources/retryButton.png")));
		retryButton.setBounds(x + 350, y + 270,100,20);
		retryButton.setActionCommand("Restart");
		retryButton.addActionListener(listener);
		retryButton.setFocusable(false);
		panel.add(retryButton);

		returnButton = new JButton(new ImageIcon(getClass().getResource("/resources/backToMenuButton.png")));
		returnButton.setBounds(x + 350, y + 315, 100, 20);
		returnButton.setActionCommand("Return");
		returnButton.addActionListener(listener);
		returnButton.setFocusable(false);
		panel.add(returnButton);

	}
	
	public void paintGameOver(Graphics2D g2d) {
		
		g2d.drawImage(icon.getImage(), x, y, null);
		
	}
	
	public void move(int x, int y) {
		this.x = x;
		this.y = y;
		
		retryButton.setBounds(x + 350, y + 270, 100, 20);
		returnButton.setBounds(x + 350, y + 315, 100, 20);
	}
	

	
	public int getX() {
		return x;
	}
	
	
	public int getY() {
		return y;
	}
	

}
