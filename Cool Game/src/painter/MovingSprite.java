package painter;

import players.Direction;

/**
 * Used for moving the sprite around
 * @author Asheld
 *
 */

public abstract class MovingSprite extends Sprite {
	
	/**
	 * The current y velocity
	 */
	private double veloY = 0;
	private final double gravity = 500.0; /// May want to move this to state later
	private double delta = 0;
	
	/**
	 * The movement speed of the sprite
	 */
	private int movementSpeed = 5;
	private int xPos, yPos;
	
	private int coolDown = 0;
	
	/**
	 * The direction of which the player is facing
	 */
	private Direction direction;
	
	public MovingSprite(int x, int y) {
		super(x, y);
		xPos = getX();
		yPos = getY();
		direction = Direction.RIGHT;
	}
	
	/**
	 * Move the sprite towards a direction
	 * @param direction The direction
	 */
	public void move(Direction direction) {
		
		switch(direction) {
		case LEFT:
			xPos -= movementSpeed;
			setX(xPos);
			break;

		case RIGHT:
			xPos += movementSpeed;
			setX(xPos);
			break;

		case UP:
			veloY = -300;
			break;
			
		default: 
			break;
		}
	}
	
	/**
	 * Set a new direction for the player
	 * @param direction The direction
	 */
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	
	/**
	 * Return the direction the player is currently facing
	 * @return direction The direction
	 */
	public Direction getDirection() {
		return direction;
	}
	
	/**
	 * Get the movement speed of the sprite
	 * @return movementSpeed The movement speed
	 */
	public int getSpeed() {
		return movementSpeed;
	}
	
	/**
	 * Get the current cool down time for the sprite
	 * @return
	 */
	public int getCoolDown() {
		return coolDown;
	}
	
	/**
	 * Get the current y velocity for the sprite
	 * @return yVelo
	 */
	public double getVeloY() {
		return (veloY * delta);
	}
	
	/**
	 * Set a new y velocity for the sprite
	 * @param yVelo
	 */
	public void setVeloY(double yVelo) {
		this.veloY = yVelo;
	}
	
	/**
	 * Set a new movement speed for the sprite
	 * @param movementSpeed The speed
	 */
	public void setSpeed(int movementSpeed) {
		this.movementSpeed = movementSpeed;
	}
	
	/**
	 * Set a cooldown period for the sprite
	 * @param coolDown
	 */
	public void setCoolDown(int coolDown) {
		this.coolDown = coolDown;
	}
	
	/**
	 * Update the sprite
	 */
	public void update(int TPS) {

		coolDown = coolDown >= 1 ? coolDown - 1 : coolDown;

		delta = TPS / 2000.0;
		yPos = getY();
		setY(yPos += veloY * delta);
		veloY += gravity * delta;

	}

	
	
}
