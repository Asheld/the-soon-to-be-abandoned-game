package painter;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import monsters.Monster;
import players.Player;
import terrain.Block;
import threading.Tickable;
import engine.GameState;

@SuppressWarnings("serial")
public class DrawPanel extends JPanel implements Tickable {

	private GameState gameState;

	private StartMenu startMenu;
	private GameOverMenu gameOverMenu;

	private int transitX = 0;
	private int transitY = 0;
	private int transitMenuX = 0;
	private int transitMenuY = 0;

	private int healthBarX = 800 - 120;
	private int healthBarY = 20;
	private int healthWidth = 100;
	private int healthHeight = 10;

	private boolean drawStartMenu = false;
	private boolean gameOver = false;
	private boolean retry = false;
	private boolean backToMenu = false;
	private boolean transit = false;



	public DrawPanel(GameState state) {

		gameState = state;

		setLayout(null);

		/// Create the start menu on initialization
		startMenu(0,0);
		
	}


	/**
	 * Repainting the game with every tick.
	 */
	@Override
	public void tick() {

		if (transit && drawStartMenu) {
			
			if (!backToMenu) {
				if (transitX < gameState.getWindowSize().width) {
					transitX += 15;
					startMenu.move(startMenu.getX() - 15, 0);
				} else {
					removeAll();
					drawStartMenu = false;
					transit = false;
					revalidate();
				}
			}

			if (backToMenu) {
				if (transitX > 0) {
					transitX -= 15;
					startMenu.move(startMenu.getX() + 15, 0);
					gameOverMenu.move(gameOverMenu.getX() + 15, 0);
				} else {
					transit = false;
					backToMenu = false;
					gameOver = false;
					transitX = 0;
				}
				
			}
			

				
		} else if (transit && gameOver) {

			if (!retry) {
					if (transitMenuY < 0) {
						transitMenuY += 15;
						transitY += 15;
						gameOverMenu.move(0, transitMenuY);
					} else {
						transit = false;
						revalidate();
					}
		}

			if (retry) {

				if (transitMenuY > -gameState.getWindowSize().height) {
					transitMenuY -= 15;
					transitY -= 15;
					gameOverMenu.move(0, transitMenuY);
				} else {
					transit = false;
					gameOver = false;
					retry = false;
					removeAll();
					revalidate();
				}
			}
			
		} 

		repaint();
	}

	/**
	 * Draw all of the objects currently in game
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;
		
		AffineTransform oldForm = g2d.getTransform();
	   
		if (drawStartMenu) {
		startMenu.paintStartMenu(g2d);
	
		g2d.translate(gameState.getWindowSize().width - transitX, transitY);
		}
		
		if (gameOver) {
			
			gameOverMenu.paintGameOver(g2d);
			
			g2d.translate(0, 0 + transitY);
		}

		gameWindow(g2d);

		g2d.setTransform(oldForm); 
		
	}

	/**
	 * Create the start menu that appears upon initialization of the game
	 * @param x The x location of where it should be drawn
	 * @param y The y location of where it should be drawn
	 */
	private void startMenu(int x, int y) {
		
		ActionListener listener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				switch (e.getActionCommand()) {
				
				case "Start":
					gameState.getStageManager().constructStage(1, 1);
					transit = true;
					break;
					
				case "Continue":
					/// Continue will probably be implemented much later
					break;
				
				case "Exit":
					System.exit(0);
					break;
				}
			}
			
			
		};
		
		startMenu = new StartMenu(this, listener, x, y);
		drawStartMenu = true;

	}
	
	/**
	 * Draw the contents of the game window
	 * @param g2d
	 */
	private void gameWindow(Graphics2D g2d) {

		AffineTransform oldForm = g2d.getTransform();

		translateScene(g2d);

		if (!gameOver) { 
			List<Player> players = gameState.getPlayers();
			synchronized(players) {
				for (Player p : players) {
					p.drawSprite(g2d);
				}

			}
		}

		List<Monster> monsters = gameState.getMonsters();
		synchronized(monsters) {
			for (Monster m : monsters) {
				m.drawSprite(g2d);
			}
		}

		List<Block> terrain = gameState.getTerrain();
		synchronized(terrain) {
			for (Block b : terrain) { 
				b.drawSprite(g2d);
			}
		}


		g2d.setTransform(oldForm); 

		drawHealth(g2d);

	}
	

	/**
	 * Draw a health bar representing the players current hp
	 * @param g2d
	 */
	private void drawHealth(Graphics2D g2d){
		if(gameState.getPlayer().getMaxHP() > 0){
			g2d.drawRect(healthBarX, healthBarY, healthWidth, healthHeight);
			g2d.setColor(Color.RED);
			g2d.fillRect(healthBarX +1, healthBarY +1, healthWidth-1, healthHeight-1);
			g2d.setColor(Color.GREEN);
			double healthRemaining = (double)(gameState.getPlayer().getHP()) / gameState.getPlayer().getMaxHP();
			g2d.fillRect(healthBarX +1, healthBarY+1, (int)((healthWidth - 1) * healthRemaining) ,healthHeight-1);
		}
	}
	
	public void setGameOver() {
		transitMenuX = 0;
		transitMenuY = -gameState.getStageManager().getStageSize().height;
		
		ActionListener listener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				switch (e.getActionCommand()) {
				case "Restart":
					gameState.getStageManager().deconstructStage();
					gameState.getStageManager().constructStage(gameState.getStageManager().getCurrentStage(), 1);	
					retry = true;
					transit = true;
					break;
					
				case "Return":
					gameState.getStageManager().deconstructStage();
					startMenu(-800, 0);
					backToMenu = true;
					transit = true;
					break;
				}
			}
			
		};
		
		gameOverMenu = new GameOverMenu(this, listener, 0, transitMenuY);
		gameOver = true;
		transit = true;
	}

	/**
	 * Translate the scene 
	 * @param g2d
	 */
	private void translateScene(Graphics2D g2d) {

		int x = gameState.getPlayer().getX();
		int y = gameState.getPlayer().getY();

		int windowWidth = gameState.getWindowSize().width / 2;
		int windowHeight = gameState.getWindowSize().height / 2;

		int playerWidth = gameState.getPlayer().getWidth();
		int playerHeight = gameState.getPlayer().getHeight();

		int stageWidth = gameState.getStageManager().getStageSize().width;

		if (x < windowWidth) {
			g2d.translate(-playerWidth, -y + windowHeight - playerHeight);
		} else if (x > stageWidth - windowWidth) {
			g2d.translate(-stageWidth + windowWidth * 2 - playerWidth, -y + windowHeight - playerHeight);
		}
		else { 
			g2d.translate(-x + windowWidth - playerWidth, -y + windowHeight - playerHeight);
		}

	}

}
