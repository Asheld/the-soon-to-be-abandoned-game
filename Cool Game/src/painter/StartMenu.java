package painter;

import java.awt.Graphics2D;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class StartMenu {
	
	private JButton startButton, continueButton, exitButton;
	
	private int x, y;
	private ImageIcon icon = new ImageIcon(getClass().getResource("/resources/titleScreen.png"));	/// Title background
	
	public StartMenu(JPanel panel, ActionListener listener, int x, int y) {
		
		this.x = x;
		this.y = y;
		
		startButton = new JButton(new ImageIcon(getClass().getResource("/resources/start01.png")));
		startButton.setBounds(x + 350, y + 270,100,20);
		startButton.setActionCommand("Start");
		startButton.addActionListener(listener);
		startButton.setFocusable(false);
		panel.add(startButton);
		
		continueButton = new JButton(new ImageIcon(getClass().getResource("/resources/continue01.png")));
		continueButton.setBounds(x + 350,y + 315,100,20);
		continueButton.setActionCommand("Continue");
		continueButton.addActionListener(listener);
		continueButton.setFocusable(false);
		panel.add(continueButton);
		
		exitButton = new JButton(new ImageIcon(getClass().getResource("/resources/exit01.png")));
		exitButton.setBounds(x + 350,y + 360,100,20);
		exitButton.setActionCommand("Exit");
		exitButton.addActionListener(listener);
		exitButton.setFocusable(false);
		panel.add(exitButton);
		
	}
	
	
	public void paintStartMenu (Graphics2D g2d) {
		
		g2d.drawImage(icon.getImage(), x, y, null);
		
	}
	
	
	public void move(int x, int y) {
		this.x = x;
		this.y = y;
		startButton.setBounds(x + 350, y + 270, 100, 20);
		continueButton.setBounds(x + 350, y + 315, 100, 20);
		exitButton.setBounds(x + 350, y + 360, 100, 20);
		
	}
	
	public int getX() {
		return x;
	}

}
